import App.Connections.AzureDB as dbs
import App.Connections.MongoDB as mgo
import unittest

class TestConnections(unittest.TestCase):
    def test_canopyDB(self):
        sql = """SELECT TOP 100 * FROM dbo.WtiAggregateSector"""
        cnxn = dbs.CanopyDB()
        results = cnxn.select(sql)
        cnxn.close()
        self.assertEqual(len(results), 100)

    def test_fundamentalsDB(self):
        sql = """SELECT TOP 100 * FROM dbo.ConstituentFundamentals"""
        cnxn = dbs.FundamentalsDB()
        results = cnxn.select(sql)
        cnxn.close()
        self.assertEqual(len(results), 100)

    def test_devDB(self):
        sql = """SELECT TOP 100 * FROM devDB.kmar.BioRevFund_PureEW_DropLowMktCapIndexReturns"""
        cnxn = dbs.DevDB()
        results = cnxn.select(sql)
        cnxn.close()
        print(results[0])
        self.assertEqual(len(results), 100)

    def test_factsetDB(self):
        sql = """SELECT TOP 100 * FROM FactSetDB.sym_v1.sym_sedol"""
        cnxn = dbs.FactSetDB()
        results = cnxn.select(sql)
        cnxn.close()
        print(results[0])
        self.assertEqual(len(results), 100)
