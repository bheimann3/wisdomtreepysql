import os
import pymongo


class MongoDB():
    """Base class for MongoDB Connections"""

    USER = None
    PASSWD = None
    HOST = None
    REPLICA_SET = None
    DB_NAME = None
    PORT = None
    client = None

    def __init__(self):
        # Create the connection string (always uses SSL)
        connection_string = "mongodb://" + self.USER + ":" + self.PASSWD + "@" + self.HOST + \
                            "/" + self.DB_NAME + "?ssl=true&replicaSet=" + \
                            self.REPLICA_SET + "&authSource=admin"
        # Connect to MongoDB
        self.client = pymongo.MongoClient(connection_string)

    def find(self, collection, query):
        """Generic find method

        Generic find method that accepts a mongo shell query for execution against the collection
        passed as a parameter and returns a list of docs.
        """

        conn = self.client[self.DB_NAME][collection]
        result_cursor = conn.find(query)
        results = [doc for doc in result_cursor]
        return results


# Class dedicated to PortfolioEngine DB inside general cluster
class PortfolioEngine(MongoDB):
    """PortfolioEngine connection class"""

    DB_NAME = 'portfolioengine'
    USER = os.getenv('MONGODB_USER', "NA")
    PASSWD = os.getenv('MONGODB_PASSWD', "NA")
    HOST = os.getenv('MONGODB_SERVER_NAME', "NA")
    REPLICA_SET = os.getenv('MONGODB_REPLICA_SET', "NA")
    PORT = int(os.getenv("MONGODB_PORT", "0"))
