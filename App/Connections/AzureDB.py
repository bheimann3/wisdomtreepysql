import pyodbc
import pandas as pd
from App.Connections import Servers
import netrc
##############
# Azure SQL DB
##############


CONNECTION_STRING = "DRIVER={{ODBC Driver 17 for SQL Server}};SERVER={};DATABASE={};UID={};PWD={};MARS_Connection=Yes"


class AzureSQLDatabase(object):
    """Base class for Azure SQL DB Connections"""

    connection_string = ""

    def __init__(self):
        self.connection = None
        self.cursor = None

    def __get_conn(self):
        if self.connection == None:
            self.connection = pyodbc.connect(self.connection_string)
        return self.connection

    def __get_cursor(self):
        if self.cursor == None:
            self.cursor = self.__get_conn().cursor()
        return self.cursor

    def query(self, query, params=None):
        if params:
            return self.__get_cursor().execute(query, params)
        else:
            return self.__get_cursor().execute(query)

    def querymany(self, query, params):
        if params:
            return self.__get_cursor().executemany(query, params)
        else:
            return self.__get_cursor().executemany(query)

    def select(self, query, params=None, format: str = 'raw'):
        """Execute raw SQL with a format switch to indicate how the results should be returned"""

        # if not a pandas dataframe call then spit the cursor and fetch the results directly
        if format != 'pd':
            if params:
                result_cursor = self.__get_cursor().execute(query, params)
            else:
                result_cursor = self.__get_cursor().execute(query)

            data_results = result_cursor.fetchall()

            # raw switch means the cursor results will be returned as is
            if format == 'raw':
                return data_results

            # dict with two keys--> "columns", and "data" as a list of lists
            elif format == 'dict_horz':
                cols = [x[0] for x in result_cursor.description]
                results_dict = dict(zip(['data', 'columns'], [data_results, cols]))
                return results_dict
            elif format == 'dict_vert':
                # dict with key:values pair per column
                return

        # pd switch to read sql directly into a pandas dataframe
        elif format == 'pd':
            results_dataframe = pd.read_sql(query, self.__get_conn())
            return results_dataframe

    def commit(self):
        return self.__get_conn().commit()

    def close(self):
        if self.connection:
            try:
                self.connection.close()
            except:
                pass
            self.connection = None

    def __del__(self):
        self.close()


# CanopyDB
class CanopyDB(AzureSQLDatabase):
    """CanopyDB connection class"""
    server = Servers.CANOPY
    __uid, __acct, __pwd = netrc.netrc().authenticators(server)
    connection_string = CONNECTION_STRING.format(
        server,
        'CanopyDB',
        __uid,
        __pwd)



# D01 DBs
class FundamentalsDB(AzureSQLDatabase):
    """FundamentalsDB connection class"""
    server = Servers.D01
    __uid, __acct, __pwd = netrc.netrc().authenticators(server)
    connection_string = CONNECTION_STRING.format(
        server,
        'FundamentalsDB',
        __uid,
        __pwd)


class DevDB(AzureSQLDatabase):
    """DevDB connection class"""
    server = Servers.D01
    __uid, __acct, __pwd = netrc.netrc().authenticators(server)
    connection_string = CONNECTION_STRING.format(
        server,
        'DevDB',
        __uid,
        __pwd)


class new_connDB(AzureSQLDatabase):
    """FundamentalsDB connection class"""
    server = Servers.P13
    __uid, __acct, __pwd = netrc.netrc().authenticators(server)
    connection_string = CONNECTION_STRING.format(
        server,
        'FundamentalsDB',
        __uid,
        __pwd)


class ModelsDB(AzureSQLDatabase):
    """ModelsDB connection class"""
    server = Servers.P11
    __uid, __acct, __pwd = netrc.netrc().authenticators(server)
    connection_string = CONNECTION_STRING.format(
        server,
        'ModelsDB',
        __uid,
        __pwd)


class FactSetDB(AzureSQLDatabase):
    """DevDB connection class"""
    server = Servers.D01
    __uid, __acct, __pwd = netrc.netrc().authenticators(server)
    connection_string = CONNECTION_STRING.format(
        server,
        'FactSetDB',
        __uid,
        __pwd)


# P11 DBs
class ModelSolutionsDB(AzureSQLDatabase):
    """ModelSolutionsDB connection class"""
    server = Servers.P11
    __uid, __acct, __pwd = netrc.netrc().authenticators(server)
    connection_string = CONNECTION_STRING.format(
        server,
        'ModelSolutionsDB',
        __uid,
        __pwd)

class PerformanceDB(AzureSQLDatabase):
    """PerformanceDB connection class"""
    server = Servers.P11
    __uid, __acct, __pwd = netrc.netrc().authenticators(server)
    connection_string = CONNECTION_STRING.format(
        server,
        'PerformanceDB',
        __uid,
        __pwd)


# P13 DBs
class ResearchDB(AzureSQLDatabase):
    """ResearchDB connection class"""
    server = Servers.P11
    __uid, __acct, __pwd = netrc.netrc().authenticators(server)
    connection_string = CONNECTION_STRING.format(
        server,
        'ResearchDB',
        __uid,
        __pwd)


class LipperDB(AzureSQLDatabase):
    """LipperDB connection class"""
    server = Servers.P13
    __uid, __acct, __pwd = netrc.netrc().authenticators(server)
    connection_string = CONNECTION_STRING.format(
        server,
        'LipperDB',
        __uid,
        __pwd)


class FundamentalsDB13(AzureSQLDatabase):
    """FundamentalsDB connection class"""
    server = Servers.P13
    __uid, __acct, __pwd = netrc.netrc().authenticators(server)
    connection_string = CONNECTION_STRING.format(
        server,
        'FundamentalsDB',
        __uid,
        __pwd)