# WisdomTreePySql

This is a package for accessing the database with python. It requires a .netrc file for authentication. 

####  To use install to your environment with the line below:
```
pip install git+ssh://bheimann3@bitbucket.org/bheimann3/wisdomtreepysql.git
```
